import {showModal} from './modal'
import {startFight} from '../fighterSelector'
interface Ifighter {
  _id: string
  name: string
  health: number 
  attack: number 
  defense: number
  source: string
}

import {listenerCall} from '../fight'
export function showWinnerModal (fighter: Ifighter, fighters: Ifighter[]){
  // call showModal function 
  let showWinnerHtml = function(fighter: Ifighter){
    let winnerHtml = document.createElement('div');
    winnerHtml.className = "winner-block";
    winnerHtml.insertAdjacentHTML('afterbegin', `<img class="winner-block_img" src="${fighter.source}" alt="winner">`)
    return winnerHtml
  }
  const restart = () => {
    return startFight(fighters)
  }
  showModal({title: `Winner: ${fighter.name}`, bodyElement: showWinnerHtml(fighter), onClose: restart});
}
export function massageModal(){
  let massageHtml = document.createElement('div');
  massageHtml.className = "massage_block";
  massageHtml.insertAdjacentHTML('afterbegin', `<p class="massage_block__text">Wait for champion power recovering</p>`)
  const root_elem = <HTMLElement>document.getElementById('root')
  root_elem.append(massageHtml);
}
