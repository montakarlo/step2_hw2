import { createElement } from '../helpers/domHelper';
import {getFighterInfo} from './fighterSelector'
interface Ifirst {
  tagName: string
  className: string
}
interface Ifighter {
  _id: string
  name: string
  health: number 
  attack: number 
  defense: number
  source: string
}
// interface Ifighter {
//   [key: string]: string | number
// }

export function createFighterPreview(fighter: Ifighter, position: string) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  // fighterElement.append(createFighterHtml())
  let createFighterHtml = function(fighter: Ifighter){
    let fighterkeys = Object.keys(fighter).reverse()
    let fighterDiv = document.createElement('div');
    fighterDiv.className = "fighter-info";
    // fighterkeys.forEach(element => {
    //   if (element != '_id' && element != 'source'){
    //     fighterDiv.insertAdjacentHTML('afterbegin', `<div class="fighter-info_detales"><p class="fighter-info_detales__title">${element}: </p> <p class="fighter-info_detales__value">${fighter.element}</p></div>`)
    //   }
    // });
    fighterDiv.insertAdjacentHTML('afterbegin', `<div class="fighter-info_detales"><p class="fighter-info_detales__title">Name: </p> <p class="fighter-info_detales__value">${fighter.name}</p></div>`)
    fighterDiv.insertAdjacentHTML('afterbegin', `<div class="fighter-info_detales"><p class="fighter-info_detales__title">Health: </p> <p class="fighter-info_detales__value">${fighter.health}</p></div>`)
    fighterDiv.insertAdjacentHTML('afterbegin', `<div class="fighter-info_detales"><p class="fighter-info_detales__title">Attack: </p> <p class="fighter-info_detales__value">${fighter.attack}</p></div>`)
    fighterDiv.insertAdjacentHTML('afterbegin', `<div class="fighter-info_detales"><p class="fighter-info_detales__title">Defense: </p> <p class="fighter-info_detales__value">${fighter.defense}</p></div>`)

    fighterElement.append(fighterDiv, createFighterImage(fighter))
  }
  createFighterHtml(fighter);

  return fighterElement;
}

export function createFighterImage(fighter: Ifighter) {

  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
