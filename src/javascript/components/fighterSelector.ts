import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { callApi } from '../helpers/apiHelper';
import {fighterService} from '../services/fightersService'

import { getDamage } from './fight';
interface Ifighter {
  _id: string
  name: string
  health: number 
  attack: number 
  defense: number
  source: string
}
export function createFightersSelector() {
  let selectedFighters = <Ifighter []> [];

  return async (event: KeyboardEvent, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

let stop = false


export async function getFighterInfo(fighterId: string) {

  let champ_info_obj = await fighterService.getFighterDetails(fighterId)
  Object.keys(champ_info_obj).forEach(element => {
    fighterDetailsMap.set(element, champ_info_obj[element]);
  });
  console.log(`Map is here: `, fighterDetailsMap)
  return champ_info_obj
}

function renderSelectedFighters(selectedFighters: Ifighter []) {
  const fightersPreview = <HTMLElement>document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;

  if (!stop){
    if (playerTwo){
      const secondPreview = createFighterPreview(playerTwo, 'right');
      const versusBlock = createVersusBlock(selectedFighters);
      fightersPreview.append(versusBlock, secondPreview);
      stop = true
    }else if (playerOne){
      const firstPreview = createFighterPreview(playerOne, 'left');
      fightersPreview.append(firstPreview);
    }
  }
}

export let fightersForFight: Ifighter[] = [];

function createVersusBlock(selectedFighters: Ifighter[]) {
  fightersForFight.push(selectedFighters[0], selectedFighters[1]);
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);
  return container;
}

export function startFight(selectedFighters: Ifighter []) {
  renderArena(selectedFighters);
}
