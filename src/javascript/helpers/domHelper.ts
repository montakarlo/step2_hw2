interface Iattr {
  [key:string]:string
}

interface Iattributes {
  tagName: string
  className: string
  attributes?: Iattr
}
export function createElement({ tagName, className, attributes = {} }: Iattributes) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
