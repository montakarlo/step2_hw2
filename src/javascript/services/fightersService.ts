import { callApi } from '../helpers/apiHelper';
import {fightersDetails} from '../helpers/mockData'

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try{
      return callApi(`details/fighter/${id}.json`,FighterService.getFighters)
    } catch(error){
      throw error;
    }
  }
}

export const fighterService = new FighterService();
