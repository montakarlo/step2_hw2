import { controls } from '../../constants/controls';
let [leftHit, rightHit, leftBlock, rightBlock, leftCritical, rightCritical, rightBlockFromCritical, leftBlockFromCritical] = [false, false, false, false, false, false, false, false];
let [leftHit_value, rightHit_value, leftBlock_value, rightBlock_value, leftCritical_value, rightCritical_value] = [0, 0, 0, 0, 0, 0];
let firstPlayerHealth;
let secondPlayerHealth;
export let leftCriticalTimer_boolean = false;
export let rightCriticalTimer_boolean = false;
let finishGame = false;
const left_fighter_img = document.querySelector('.arena___left-fighter>img');
const right_fighter_img = document.querySelector('.arena___right-fighter>img');
const left_fighter = document.querySelector('.arena___left-fighter');
const right_fighter = document.querySelector('.arena___right-fighter');
const left_hit = document.querySelector('.arena___hit-left');
const right_hit = document.querySelector('.arena___hit-right');
const left_block = document.querySelector('.arena___block-left');
const right_block = document.querySelector('.arena___block-right');
const left_hit_critical = document.querySelector('.arena___critical-hit-left');
const right_hit_critical = document.querySelector('.arena___critical-hit-right');
export async function fight(firstFighter, secondFighter) {
    // export async function fight(firstFighter: Ifighter, secondFighter: Ifighter):Promise<{firstFighter: Ifighter, secondFighter: Ifighter}> {
    [leftHit, rightHit, leftBlock, rightBlock, leftCritical, rightCritical, rightBlockFromCritical, leftBlockFromCritical] = [false, false, false, false, false, false, false, false];
    [leftHit_value, rightHit_value, leftBlock_value, rightBlock_value, leftCritical_value, rightCritical_value] = [0, 0, 0, 0, 0, 0];
    leftCriticalTimer_boolean = false;
    rightCriticalTimer_boolean = false;
    finishGame = false;
    firstPlayerHealth = firstFighter.health;
    secondPlayerHealth = secondFighter.health;
    console.log("Hit power: " + getHitPower(firstFighter), "Block power: " + getBlockPower(firstFighter));
    console.log("Hit power: " + getHitPower(secondFighter), "Block power: " + getBlockPower(secondFighter));
    // addKeysEvents(firstFighter, secondFighter);
    document.addEventListener('keyup', listenerCallUp(firstFighter, secondFighter));
    document.addEventListener('keydown', listenerCall(firstFighter, secondFighter));
    console.log("started");
    return new Promise((resolve) => {
        document.addEventListener('keydown', function () {
            if ((firstPlayerHealth <= 0 || secondPlayerHealth <= 0) && finishGame == false) {
                // console.log('modal')
                finishGame = true;
                if (firstPlayerHealth <= 0) {
                    resolve(secondFighter);
                    left_fighter_img.style.display = 'none';
                }
                if (secondPlayerHealth <= 0) {
                    resolve(firstFighter);
                    right_fighter_img.style.display = 'none';
                }
            }
        });
    });
}
// resolve the promise with the winner when fight is over
export function getDamage(attacker, defender) {
    let rightHealthIndicator = document.querySelector('.arena___health-bar-right');
    let leftHealthIndicator = document.querySelector('.arena___health-bar-left');
    let lefttHit_value = getHitPower(attacker);
    let rightBlock_value = getBlockPower(defender);
    let rightHit_value = getHitPower(defender);
    let leftBlock_value = getBlockPower(attacker);
    let currentLeftHealthPerCent = (firstPlayerHealth * 100) / attacker.health;
    let currentRightHealthPerCent = (secondPlayerHealth * 100) / defender.health;
    if (leftHit && rightBlock == false && lefttHit_value > rightBlock_value) {
        secondPlayerHealth = secondPlayerHealth - lefttHit_value + rightBlock_value;
        healthChanger();
        healthColor();
        return console.log(lefttHit_value - rightBlock_value);
    }
    else if (leftHit && rightBlock == false && lefttHit_value <= rightBlock_value) {
        return console.log(0);
    }
    if (rightHit && leftBlock == false && rightHit_value > leftBlock_value) {
        firstPlayerHealth = firstPlayerHealth - rightHit_value + leftBlock_value;
        healthChanger();
        healthColor();
        return console.log(rightHit_value - leftBlock_value);
    }
    else if (rightHit && leftBlock == false && rightHit_value <= leftBlock_value) {
        return console.log(0);
    }
    if (leftCritical || rightCritical) {
        rightCritical_value = 2 * defender.attack;
        leftCritical_value = 2 * attacker.attack;
        if (leftCritical) {
            secondPlayerHealth = secondPlayerHealth - leftCritical_value;
        }
        if (rightCritical) {
            firstPlayerHealth = firstPlayerHealth - rightCritical_value;
        }
        healthChanger();
        healthColor();
    }
    function healthChanger() {
        if (firstPlayerHealth <= 0) {
            leftHealthIndicator.style.width = `0px`;
        }
        else {
            leftHealthIndicator.style.width = `${(firstPlayerHealth * 100) / attacker.health}%`;
        }
        if (secondPlayerHealth <= 0) {
            rightHealthIndicator.style.width = '0px';
        }
        else {
            rightHealthIndicator.style.width = `${(secondPlayerHealth * 100) / defender.health}%`;
        }
    }
    function healthColor() {
        if (currentLeftHealthPerCent < 67) {
            leftHealthIndicator.style.backgroundColor = 'orange';
        }
        ;
        if (currentLeftHealthPerCent < 33) {
            leftHealthIndicator.style.backgroundColor = 'red';
        }
        ;
        if (currentRightHealthPerCent < 67) {
            rightHealthIndicator.style.backgroundColor = 'orange';
        }
        ;
        if (currentRightHealthPerCent < 33) {
            rightHealthIndicator.style.backgroundColor = 'red';
        }
    }
}
function valuesCleaner() {
    leftHit_value = 0;
    rightHit_value = 0;
    leftBlock_value = 0;
    rightBlock_value = 0;
    leftCritical_value = 0;
    rightCritical_value = 0;
}
export function getHitPower(fighter) {
    let criticalHitChance = getRandomFloat(1, 2);
    // let criticalHitChance = 1
    let hit_power = fighter.attack * criticalHitChance;
    return hit_power;
}
export function getBlockPower(fighter) {
    let dodgeChance = getRandomFloat(1, 2);
    // let dodgeChance = 1
    let block_powwer = fighter.defense * dodgeChance;
    return block_powwer;
}
export function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}
export function listenerCall(firstFighter, secondFighter) {
    return function keyPressFunction(event) {
        if (firstPlayerHealth > 0 && secondPlayerHealth > 0) {
            // let fightWasStarted = false;
            let [KeyQ, KeyW, KeyE] = controls.PlayerOneCriticalHitCombination;
            let [KeyU, KeyI, KeyO] = controls.PlayerTwoCriticalHitCombination;
            switch (event.code) {
                //Left
                case `${controls.PlayerOneAttack}`:
                    if (leftBlock == false && leftCritical == false) {
                        leftHit = true;
                        getDamage(firstFighter, secondFighter);
                        valuesCleaner();
                        left_fighter.style.left = `${document.documentElement.clientWidth - 600}px`;
                        left_hit.style.display = 'block';
                    }
                    break;
                case `${controls.PlayerOneBlock}`:
                    if (leftHit == false && leftCritical == false && leftBlockFromCritical == false) {
                        leftBlock = true;
                        getDamage(firstFighter, secondFighter);
                        valuesCleaner();
                        left_fighter.style.bottom = '960px';
                        left_block.style.display = 'block';
                    }
                    break;
                case KeyQ || KeyW || KeyE:
                    runOnKeys(() => {
                        if (leftHit == false && leftBlock == false && leftCritical == false && leftCriticalTimer_boolean == false) {
                            leftCriticalTimer_boolean = true;
                            leftCriticalTimer();
                            leftCritical = true;
                            rightBlockFromCritical = true;
                            getDamage(firstFighter, secondFighter);
                            valuesCleaner();
                            left_fighter.style.left = `${document.documentElement.clientWidth - 600}px`;
                            left_fighter.style.transition = "all 0.1s";
                            left_fighter_img.style.height = '520px';
                            left_fighter_img.style.transition = "all 0.1s";
                            left_hit_critical.style.display = 'block';
                        }
                    }, KeyQ, KeyW, KeyE);
                    break;
                ///Right
                case `${controls.PlayerTwoAttack}`:
                    if (rightBlock == false && rightCritical == false) {
                        rightHit = true;
                        getDamage(firstFighter, secondFighter);
                        valuesCleaner();
                        right_fighter.style.right = `${document.documentElement.clientWidth - 600}px`;
                        right_hit.style.display = 'block';
                    }
                    break;
                case `${controls.PlayerTwoBlock}`:
                    if (rightHit == false && rightCritical == false && rightBlockFromCritical == false) {
                        rightBlock = true;
                        getDamage(firstFighter, secondFighter);
                        valuesCleaner();
                        right_fighter.style.bottom = '960px';
                        right_block.style.display = 'block';
                    }
                    break;
                case KeyU || KeyI || KeyO:
                    runOnKeys(() => {
                        if (rightHit == false && rightBlock == false && rightCritical == false && rightCriticalTimer_boolean == false) {
                            rightCriticalTimer_boolean = true;
                            rightCriticalTimer();
                            rightCritical = true;
                            leftBlockFromCritical = true;
                            getDamage(firstFighter, secondFighter);
                            // console.log("Right_critical: "+rightCritical_value)
                            valuesCleaner();
                            right_fighter.style.right = `${document.documentElement.clientWidth - 600}px`;
                            right_fighter.style.transition = "all 0.05s";
                            right_fighter_img.style.height = '520px';
                            right_fighter_img.style.transition = "all 0.05s";
                            right_hit_critical.style.display = 'block';
                        }
                    }, KeyU, KeyI, KeyO);
                    break;
            }
        }
    };
}
let leftCriticalTimer = function () {
    setTimeout(() => {
        leftCriticalTimer_boolean = false;
    }, 10000);
    let count = 0;
    let intervalId = setInterval(function () {
        count++;
        if (count == 1000) {
            clearInterval(intervalId);
        }
        criticalRiser(count, '.arena___critical-bar-left');
    }, 10);
};
let rightCriticalTimer = function () {
    setTimeout(() => {
        rightCriticalTimer_boolean = false;
    }, 10000);
    let count = 0;
    let intervalId = setInterval(function () {
        count++;
        if (count == 1000) {
            clearInterval(intervalId);
        }
        criticalRiser(count, '.arena___critical-bar-right');
    }, 10);
};
function criticalRiser(i, style) {
    const cl_name = document.querySelector(style);
    cl_name.style.width = `${(i + 1) / 10}%`;
}
export function listenerCallUp(firstFighter, secondFighter) {
    return function keyPressFunction(event) {
        if (firstPlayerHealth > 0 && secondPlayerHealth > 0) {
            // let fightWasStarted = false;
            let [KeyQ, KeyW, KeyE] = controls.PlayerOneCriticalHitCombination;
            let [KeyU, KeyI, KeyO] = controls.PlayerTwoCriticalHitCombination;
            switch (event.code) {
                //Left
                case `${controls.PlayerOneAttack}`:
                    leftHit = false;
                    left_fighter.style.left = '0';
                    left_hit.style.display = 'none';
                    break;
                case `${controls.PlayerOneBlock}`:
                    leftBlock = false;
                    left_fighter.style.bottom = '550px';
                    left_block.style.display = 'none';
                    break;
                case KeyQ && KeyW && KeyE:
                    leftCritical = false;
                    rightBlockFromCritical = false;
                    left_fighter.style.left = '0';
                    left_fighter.style.transition = "all 0.2s";
                    left_fighter_img.style.height = '430px';
                    left_fighter_img.style.transition = "all 0.2s";
                    left_hit_critical.style.display = 'none';
                    break;
                case `${controls.PlayerTwoAttack}`:
                    rightHit = false;
                    right_fighter.style.right = '0';
                    right_hit.style.display = 'none';
                    break;
                case `${controls.PlayerTwoBlock}`:
                    rightBlock = false;
                    right_fighter.style.bottom = '550px';
                    right_block.style.display = 'none';
                    break;
                case KeyU && KeyI && KeyO:
                    rightCritical = false;
                    leftBlockFromCritical = false;
                    right_fighter.style.right = '0';
                    right_fighter.style.transition = "all 0.2s";
                    right_fighter_img.style.height = '430px';
                    right_fighter_img.style.transition = "all 0.2s";
                    right_hit_critical.style.display = 'none';
                    break;
            }
        }
    };
}
function runOnKeys(func, ...codes) {
    // let pressed = new Set();
    let pressed = new Set();
    document.addEventListener('keydown', function (event) {
        pressed.add(event.code);
        for (let code of codes) {
            if (!pressed.has(code)) {
                return;
            }
        }
        pressed.clear();
        func();
    });
    document.addEventListener('keyup', function (event) {
        pressed.delete(event.code);
    });
}
